import {FETCH_MESSAGE_SUCCESS, SHOW_MODAL} from "./actions";

const initialState = {
    modalShow: false,
    messages: [],
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_MODAL:
            return {...state, modalShow: action.modal};
        case FETCH_MESSAGE_SUCCESS:
            return {...state, messages: action.messages};
        default:
            return state;
    }
};

export default productsReducer;