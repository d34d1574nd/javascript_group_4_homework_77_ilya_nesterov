import axios from '../axios-api';

export const SHOW_MODAL = 'SHOW_MODAL';
export const FETCH_MESSAGE_SUCCESS = 'FETCH_MESSAGE_SUCCESS';
export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';

export const showModal = modal => ({type: SHOW_MODAL, modal});
export const fetchMessageSuccess = messages => ({type: FETCH_MESSAGE_SUCCESS, messages});
export const createMessageSuccess = () => ({type: CREATE_MESSAGE_SUCCESS});

export const fetchMessage = () => {
    return dispatch => {
        return axios.get('/message').then(
            response => dispatch(fetchMessageSuccess(response.data))
        )
    }
};

export const createMessage = productData => {
    return dispatch => {
        return axios.post('/message', productData).then(
            () => dispatch(createMessageSuccess())
        );
    };
};