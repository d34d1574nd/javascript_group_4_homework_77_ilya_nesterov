import React from 'react';

import {apiURL} from "../../constants";
import {CardImg} from "reactstrap";

const styles = {
    width: '150px',
    marginRight: '10px',
    float: 'right'
};

const ProductThumbnail = props => {
    let image = null;

    if (props.image) {
        image = apiURL + '/uploads/' + props.image;
    }

    if (props.image === 'null') {
        return null
    }

    return <CardImg src={image} style={styles} alt="Thread image" onError={image => image.target.style.display='none'}/>
};

export default ProductThumbnail;