import React, {Component} from 'react';
import {Card, CardText, CardTitle, Col, Row} from "reactstrap";

import {connect} from "react-redux";
import {fetchMessage} from "../../store/actions";

import './Thread.css'
import ProductThumbnail from "../ProductThumbnail/ProductThumbnail";

class Thread extends Component {
    componentDidMount () {
        this.props.fetchMessage()
    };

    render() {
        let messages = this.props.messages.map(item => {
            return (
                <Card className="Card" body outline color="success" key={item.id}>
                    <Row>
                        <Col xs="6">
                            <CardTitle><strong>from: </strong>{item.name}</CardTitle>
                        </Col>
                        <Col xs="6">
                            <CardTitle className="date">{item.date}</CardTitle>
                        </Col>
                    </Row>
                    <hr className="hr"/>
                    <Row>
                        <Col xs="6">
                            <CardText><strong>Message: </strong>{item.message}</CardText>
                        </Col>
                        <Col xs="6">
                            <ProductThumbnail image={item.image}/>
                        </Col>
                    </Row>
                </Card>
            )
        });
        return (
            messages
        );
    }
}

const mapStateToProps = state => ({
    messages: state.messages,
});

const mapDispatchToProps = dispatch => ({
    fetchMessage: () => dispatch(fetchMessage())
});

export default connect(mapStateToProps, mapDispatchToProps)(Thread);