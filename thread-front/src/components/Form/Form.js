import React, {Component, Fragment} from 'react';
import {
    FormGroup,
    Input,
    Label,
    Form,
    Button,
    Col,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
} from "reactstrap";

import './Form.css';
import {connect} from "react-redux";
import {createMessage, fetchMessage, showModal} from '../../store/actions';

class FormThread extends Component {
    state= {
        name: '',
        message: '',
        image: null,
        date: new Date().toUTCString()
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.createMessage(formData);
        this.setState({name: '', message: '', image: null});
        setInterval(() => this.props.fetchMessage(), 1000);
        this.props.modalShow(false)
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Fragment>
            <Button outline onClick={() => this.props.modalShow(true)}>Enter new thread</Button>
            <Modal isOpen={this.props.modal} toggle={() => this.props.modalShow(false)}>
                <Form className="form" onSubmit={this.submitFormHandler}>
                <ModalHeader>Enter new thread</ModalHeader>
                <ModalBody>
                    <FormGroup row>
                        <Label for="Name" sm={2}>Name</Label>
                        <Col sm={10}>
                            <Input
                                type="text"
                                name="name" id="name"
                                placeholder="anonymous"
                                value={this.state.name}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="message" sm={2}>Message</Label>
                        <Col sm={10}>
                            <Input
                                type="textarea"
                                id="message" name="message"
                                placeholder="Enter message"
                                required
                                value={this.state.message}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="image" sm={2}>File</Label>
                        <Col sm={10}>
                            <Input
                                type="file" name="image" id="image"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button active outline type="submit" color="primary" style={{"marginRight": "15px", "width": "100px"}}>Submit</Button>
                    <Button active outline color="danger" onClick={() => this.props.modalShow(false)}>Cancel</Button>
                </ModalFooter>
            </Form>
            </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: state.modalShow,
});

const mapDispatchToProps = dispatch => ({
    modalShow: modal => dispatch(showModal(modal)),
    createMessage: message => dispatch(createMessage(message)),
    fetchMessage: () => dispatch(fetchMessage())
});

export default connect(mapStateToProps, mapDispatchToProps)(FormThread);