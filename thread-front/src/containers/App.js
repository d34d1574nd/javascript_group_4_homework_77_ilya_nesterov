import React, { Component } from 'react';
import Form from "../components/Form/Form";
import Thread from "../components/Thread/Thread";

import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Form/>
        <Thread/>
      </div>
    );
  }
}

export default App;
