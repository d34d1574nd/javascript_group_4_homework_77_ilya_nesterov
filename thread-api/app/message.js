const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const db = require('../fileDb');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();


router.get('/', (req, res) => {
    res.send(db.getMessages());
});

router.post('/', upload.single('image'), (req, res) => {
    const message = req.body;
    message.id = nanoid();

    if (req.file) {
        message.image = req.file.filename;
    }

    if (req.body.message !== '') {
        db.createMessage(message);
        res.send({message: "OK"});
    } else {
        res.status(400).send({error: "Поле 'message' обязательно к заполнению"})
    }
});

module.exports = router;
