const fs = require('fs');

const filename = './database/db.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(filename);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },
    getMessages () {
        return data;
    },
    createMessage (message) {
        if (message.name === '') {
            message.name = 'anonymous'
        }
        data.push(message);
        this.save();
    },
    save () {
        fs.writeFileSync(filename, JSON.stringify(data, null, 2))
    }
};